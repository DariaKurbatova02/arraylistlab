//Daria Kurbatova #2043755
package com.example;

public class Point {
    public double x;
    public double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    /**
     * equals method to compare two Point objects
     */
    @Override
    public boolean equals(Object other){
        if (!(other instanceof Point)){
            return false;
        }
        Point p = (Point)other;
        return p.x == this.x && p.y == this.y;
    }
}
