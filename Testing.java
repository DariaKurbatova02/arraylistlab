//Daria Kurbatova #2043755
package com.example;
import java.util.ArrayList;
import java.util.List;

public class Testing {
    public static void main( String[] args ){
        String[] words1 = {"hello", "HELLO", "HI", "bye", "BYEBYE"};
        //test countUpperCase method
        System.out.println("countUpperCase test:");
        System.out.println(ListPractice.countUpperCase(words1));
        //test isUpperCase string method
        System.out.println("isUpperCase test");
        System.out.println(ListPractice.isUpperCase("HEFW"));
        //test getUpperCase method
        System.out.println("getUpperCase test:");
        String[] uppercase = ListPractice.getUpperCase(words1);
        for (String s : uppercase){
            System.out.println(s);
        }

        //PART 2-3
        //ArrayList part
        List<String> words = new ArrayList<String>();
        System.out.println("words size before adding:");
        System.out.println(words.size());
        for (String s : words1){
            words.add(s);
        }
        System.out.println("words size after adding");
        System.out.println(words.size());
        //test arraylist countUpperCase
        System.out.println("array list countuppercase test");
        System.out.println(ListPractice.countUpperCase(words));
        //arrayList getUpperCase
        System.out.println("ArrayList getUpperCase method test");
        List<String> upperCase = new ArrayList<String>();
        upperCase = ListPractice.getUpperCase(words);
        for (int i = 0; i<upperCase.size(); i++){
            System.out.println(upperCase.get(i));
        }
        //PART 4
        System.out.println("using contains method");
        System.out.println(words.contains("HELLO"));
        System.out.println(words.contains("helloooo"));

        //PART 5
        ArrayList<Point> points = new ArrayList<Point>();
        Point a = new Point(3, 3);
        Point b = new Point(11, 77);
        Point c = new Point(333, 777);
        points.add(a);
        points.add(b);
        points.add(c);
        Point target = new Point(11, 77);
        System.out.println("does points contain target?");
        System.out.println(points.contains(target));

    }
}
