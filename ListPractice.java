package com.example;
import java.util.ArrayList;
import java.util.List;

//Daria Kurbatova #2043755
public class ListPractice {
    /**
     * method that checks how many Strings in String array are all caps
     * @param strings Array of strings to be checked
     * @return number of Strings that are all caps
     */
    public static int countUpperCase(String[] strings){
        int count = 0;
        for(String word : strings){
            if (isUpperCase(word)){
                count++;
            }
        }
        return count;
    }
    /**
     * method that checks how many Strings in arrayList are all caps
     * @param words arrayLsit of strings to be checked
     * @return number of Strings that are all caps
     */
    public static int countUpperCase(List<String> words){
        int count = 0;
        for(int i=0; i<words.size(); i++){
            if (isUpperCase(words.get(i))){
                count++;
            }
        }
        return count;
    }
    /**
     * Method that checks if a string is all caps
     * @param s input string to be checked
     * @return true if inputted string is all caps
     */
    public static boolean isUpperCase(String s){
        if (s.matches("[A-Z]*")){
            return true;
        }
        return false;
    }
    /**
     * method that returns an array of the strings that were uppercase
     * @param strings input String array 
     * @return String array left after only keeping uppercase Strings
     */
    public static String[] getUpperCase(String[] strings){
        int uppperCaseAmount = countUpperCase(strings);
        String[] upperCase = new String[uppperCaseAmount];
        int next = 0;
        for (String s : strings){
            if (isUpperCase(s)){
                upperCase[next] = s;
                next++;
            }
        }
        return upperCase;
    }
    /**
     * method that returns an arrayList of the strings that were uppercase
     * @param words input String arrayList
     * @return String arrayList left after only keeping uppercase Strings
     */
    public static List<String> getUpperCase(List<String> words){
        List<String> upperCase = new ArrayList<String>();
        for (int i=0; i<words.size(); i++){
            if (isUpperCase(words.get(i))){
                upperCase.add(words.get(i));
            }
        }
        return upperCase;
    }
    
}
